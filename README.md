# aws-sign.cr

AWS signing for HTTP Requests in Crystal.

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  aws-sign:
    gitlab: a-watson/aws-sign.cr
```

## Usage

```crystal
require "aws-sign"
```

TODO: Write usage instructions here

## Development

TODO: Write development instructions here

## Contributing

1. Fork it (<https://gitlab.com/a-watson/aws-sign.cr/fork/new>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [a-watson](https://gitlab.com/a-watson) Adam Watson - creator, maintainer
